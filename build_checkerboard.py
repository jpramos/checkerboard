
import random as rand

N_SAMPLES = 4096
COMPLEXITY = 4
IMBALANCE = 0
FILE_NAME = 'checkerboard_s{}_c{}_i{}.data'

def build_board(s, c):

    board = []

    for n in xrange(s):

        y = rand.random()
        x = rand.random()

        for it in xrange(c):
            if it/float(c) <= x < (it+1)/float(c):
                x_val = (it % 2)
                break

        for it in xrange(c):
            if it/float(c) <= y < (it+1)/float(c):
                y_val = (it % 2)
                break

        print x, y, 1 - (x_val ^ y_val)
        board.append([x, y, 1-(x_val ^ y_val)])


    print board
    return board


def write_to_file(board, file_name):

    with open(file_name, 'w') as fd:
        for row in board:
            fd.write("{},{},{}\n".format(row[0], row[1], row[2]))

    return


if __name__ == '__main__':

    rand.seed(42)

    board = build_board(N_SAMPLES, COMPLEXITY)
    
    
    write_to_file(board, FILE_NAME.format(N_SAMPLES, COMPLEXITY, IMBALANCE))




